#!/bin/sh

# install dependencies
apk add curl > /dev/null 2>&1

# load env variables
. $(dirname "$0")/.env > /dev/null 2>&1

RESULT=$(curl -s -b PHPSESSID=$PHPSESSID \
  https://friendica.feneas.org/admin \
  | grep 'admin/queue">' \
  | sed 's#.*admin/queue">\([0-9]*\).*#\1#')

[[ "$RESULT" == "" ]] && RESULT=-1

echo '{
  "labels":{"env":"friendica"},
  "results":{"items":'$RESULT'}
}'
exit 0
